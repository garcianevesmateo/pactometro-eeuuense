import * as React from 'react'
import { VoteSliders } from './VoteSliders'
import $ from 'jquery'

 

const states = {
	AL: "Alabama",
	AK: "Alaska",
	AZ: "Arizona",
	AR: "Arkansas",
	CA: "California",
	CO: "Colorado",
	CT: "Connecticut",
	DE: "Delaware",
	DC: "District of Columbia",
	FL: "Florida",
	GA: "Georgia",
	HI: "Hawaii",
	ID: "Idaho",
	IL: "Illinois",
	IN: "Indiana",
	IA: "Iowa",
	KS: "Kansas",
	KY: "Kentucky",
	LA: "Louisiana",
	ME: "Maine",
	MD: "Maryland",
	MA: "Massachusetts",
	MI: "Michigan",
	MN: "Minnesota",
	MS: "Mississippi",
	MO: "Missouri",
	MT: "Montana",
	NE: "Nebraska",
	NV: "Nevada",
	NH: "New Hampshire",
	NJ: "New Jersey",
	NM: "New Mexico",
	NY: "New York",
	NC: "North Carolina",
	ND: "North Dakota",
	OH: "Ohio",
	OK: "Oklahoma",
	OR: "Oregon",
	PA: "Pennsylvania",
	RI: "Rhode Island",
	SC: "South Carolina",
	SD: "South Dakota",
	TN: "Tennessee",
	TX: "Texas",
	UT: "Utah",
	VT: "Vermont",
	VA: "Virginia",
	WA: "Washington",
	WV: "Wst Virginia",
	WI: "Wisconsin",
	WY: "Wyoming",
}
export class StateSelector extends React.Component {
	constructor(){
		super()
		this.state = {
			selectedState: ""
		}
	}

	onChangeSelector = (event) => {
		if($('#' + (this.state.selectedState ? this.state.selectedState : "NO_STATE"))[0]) {
			$('#' + this.state.selectedState)[0].setAttribute("stroke", "")
			$('#v' + this.state.selectedState)[0].setAttribute("stroke", "")
		}
		this.setState({selectedState: event.target.value})
	}

	componentDidUpdate() {
		if($('#' + (this.state.selectedState ? this.state.selectedState : "NO_STATE"))[0]) {
			$('#' + this.state.selectedState)[0].setAttribute("stroke", "yellow")
			$('#v' + this.state.selectedState)[0].setAttribute("stroke", "yellow")
		}
	}

	render() {
		console.log(this.props)
		return (
			<div>
				<div style={{margin:"10px"}}>
					<select onChange={this.onChangeSelector}>
						<option key="" disabled selected > Seleccione un Estado </option>
						{Object.keys(states).map(key => (
							<option key={key} value={key}>{states[key]}</option>
						))}
					</select>
				</div>
				<div>
					<VoteSliders values={this.state.selectedState === "" ? null : this.props.states[this.state.selectedState]} onChange={this.props.onChange(this.state.selectedState)} />
				</div>
			</div>
		)
	}
}