import { SvgLoader, SvgProxy } from 'react-svgmt'
import * as React from 'react'
import $ from 'jquery'
import { map } from '../map/stringMap'
import { StateSelector } from './StateSelector'

export const REPUBLICAN = "#E81B23"
export const DEMOCRAT = "#00AFF3"
export const GREEN = "#02A95C"
export const LIBERTARIAN = "#D9B214"
export const WRITE_IN = "#000"
export const NO_CONTEST = "#D3D3D3"


export class MapContainer extends React.Component {

	onChange = (regionID) => (reps, dems, greens, libs, wi) => {
		const stateObject = {...this.props.states}

		stateObject[regionID] = {
			reps,
			dems, 
			greens,
			libs,
			wi
		}

		this.props.setStates(stateObject)
	}

	componentDidUpdate() {
		Object.keys(this.props.states).forEach((regionID)=>{
			this.changeColour(regionID ,this.decideColour(regionID))
		})
		
	}


	decideColour = (regionID) => {
		const max = Math.max(this.props.states[regionID].reps, this.props.states[regionID].dems, this.props.states[regionID].greens, this.props.states[regionID].libs, this.props.states[regionID].wi)
		if (max === 0)
			return NO_CONTEST
		if (max === this.props.states[regionID].reps)
			return REPUBLICAN
		if (max === this.props.states[regionID].dems)
			return DEMOCRAT
		if (max === this.props.states[regionID].greens)
			return GREEN
		if (max === this.props.states[regionID].libs)
			return LIBERTARIAN
		return WRITE_IN
	}

	changeColour = (idSelector, colour) => {
		if(idSelector !== "DC"){
			$('#' + idSelector)[0].setAttribute('style', `fill:${colour}`)
		}
		$('#v' + idSelector)[0].setAttribute('style', `fill:${colour}`)
	}

	render() {
		return (		
			<div>
				<div className="map">
					<SvgLoader svgXML={map}>
						{Object.keys(this.props.states).map((key, index ) => (
							<SvgProxy key={index} selector={key}/>
						))}
					</SvgLoader>
				</div>
				<div>
					<StateSelector onChange={this.onChange} states={this.props.states}/>
				</div>

			</div>
		)
	}
}

