import * as React from 'react';
import { Label, Slider } from '@blueprintjs/core'; 

const labelRenderer = (value) => (`${Math.trunc(value,2)}%`)

export class VoteSliders extends React.Component {

	onDemsChange = (value) => {
		const {reps, greens, libs, wi } = {...this.props.values}
		let rep = 0
		let lib = 0
		let green = 0
		let wip = 0

		if(value + greens + reps + libs + wi > 100){
			const overflow = (value + greens + reps + libs + wi) - 100
			rep = overflow * (reps / (greens + reps + libs + wi))
			lib = overflow * (libs / (greens + reps + libs + wi))
			green = overflow * (greens / (greens + reps + libs + wi))
			wip = overflow * (wi / (greens + reps + libs + wi))
		}
		this.props.onChange(reps - rep, value, greens - green, libs - lib, wi - wip)
	}

	onLibsChange = (value) => {
		const {reps, greens, dems, wi } = {...this.props.values}
		let rep = 0
		let dem = 0
		let green = 0
		let wip = 0

		if(value + greens + reps + dems + wi > 100){
			const overflow = (value + greens + reps + dems + wi) - 100
			rep = overflow * (reps / (greens + reps + dems + wi))
			dem = overflow * (dems / (greens + reps + dems + wi))
			green = overflow * (greens / (greens + reps + dems + wi))
			wip = overflow * (wi / (greens + reps + dems + wi))
		}
		this.props.onChange(reps - rep, dems - dem, greens - green, value, wi - wip)
	}
	
	onRepsChange = (value) => {
		const {dems, greens, libs, wi } = {...this.props.values}
		let dem = 0
		let lib = 0
		let green = 0
		let wip = 0

		if(value + greens + dems + libs + wi > 100){
			const overflow = (value + greens + dems + libs + wi) - 100
			dem = overflow * (dems / (greens + dems + libs + wi))
			lib = overflow * (libs / (greens + dems + libs + wi))
			green = overflow * (greens / (greens + dems + libs + wi))
			wip = overflow * (wi / (greens + dems + libs + wi))
		}
		this.props.onChange(value, dems - dem, greens - green, libs - lib, wi - wip)
	}

	onGreensChange = (value) => {
		const {reps, dems, libs, wi } = {...this.props.values}
		let rep = 0
		let lib = 0
		let dem = 0
		let wip = 0

		if(value + dems + reps + libs + wi > 100){
			const overflow = (value + dems + reps + libs + wi) - 100
			rep = overflow * (reps / (dems + reps + libs + wi))
			lib = overflow * (libs / (dems + reps + libs + wi))
			dem = overflow * (dems / (dems + reps + libs + wi))
			wip = overflow * (wi / (dems + reps + libs + wi))
		}
		this.props.onChange(reps - rep, dems - dem, value, libs - lib, wi - wip)
	}

	onWriteInChange = (value) => {
		const {reps, greens, libs, dems } = {...this.props.values}
		let rep = 0
		let lib = 0
		let green = 0
		let dem = 0

		if(value + greens + reps + libs + dems > 100){
			const overflow = (value + greens + reps + libs + dems) - 100
			rep = overflow * (reps / ( greens + reps + libs + dems))
			lib = overflow * (libs / ( greens + reps + libs + dems))
			green = overflow * (greens / ( greens + reps + libs + dems))
			dem = overflow * (dems / ( greens + reps + libs + dems))
		}
		this.props.onChange(reps - rep, dems-dem, greens - green, libs - lib, value)
	}

	render() {
		return this.props.values ? (
			<div className="sliders">
				<Label className="slider democrats">
					<div>
						Partido Demócrata
					</div>
					<Slider 
						value={this.props.values.dems} 
						onChange={this.onDemsChange}
						max={100} 
						min={0} 
						stepSize={0.01}
						labelStepSize={10}
						showTrackFill={true}
						labelRenderer={labelRenderer}
						vertical={true}
					/>
				</Label>
				<Label className="slider republicans">
					<div>
						Partido Republicano
					</div>
					<Slider 
						value={this.props.values.reps} 
						onChange={this.onRepsChange}
						max={100} 
						min={0} 
						stepSize={0.01}
						labelStepSize={10}
						showTrackFill={true}
						labelRenderer={labelRenderer}
						vertical={true}
					/>
				</Label>
				<Label className="slider libertarians">
					<div>
						Partido Libertario
					</div>
					<Slider 
						value={this.props.values.libs} 
						onChange={this.onLibsChange}
						max={100} 
						min={0} 
						stepSize={0.01}
						labelStepSize={10}
						showTrackFill={true}
						labelRenderer={labelRenderer}
						vertical={true}
					/>
				</Label>
				<Label className="slider greens">
					<div>
						Partido Verde
					</div>
					<Slider 
						value={this.props.values.greens} 
						onChange={this.onGreensChange}
						max={100} 
						min={0} 
						stepSize={0.01}
						labelStepSize={10}
						showTrackFill={true}
						labelRenderer={labelRenderer}
						vertical={true}
					/>
				</Label>
				<Label className="slider write-ins">
					<div>
						Opción Escrita a Mano
					</div>
					<Slider 
						value={this.props.values.wi} 
						onChange={this.onWriteInChange}
						max={100} 
						min={0} 
						stepSize={0.01}
						labelStepSize={10}
						showTrackFill={true}
						labelRenderer={labelRenderer}
						vertical={true}
					/>
				</Label>
			</div>
		) : null
	}
}