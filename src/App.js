import './App.css';
import { MapContainer } from './form/MapContainer';
import { ResultShower } from './results/ResultShower';
import { useState } from 'react';
import { Button, Drawer, Intent } from '@blueprintjs/core';


function App() {
  const [states, setStates] = useState({
    AL: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    AK: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    AZ: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    AR: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    CA: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    CO: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    CT: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    DE: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    DC: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    FL: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    GA: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    HI: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    ID: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    IL: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    IN: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    IA: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    KS: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    KY: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    LA: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    ME: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    MD: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    MA: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    MI: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    MN: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    MS: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    MO: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    MT: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    NE: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    NV: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    NH: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    NJ: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    NM: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    NY: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    NC: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    ND: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    OH: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    OK: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    OR: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    PA: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    RI: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    SC: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    SD: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    TN: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    TX: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    UT: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    VT: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    VA: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    WA: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    WV: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    WI: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
    WY: {
      reps: 0,
      dems: 0,
      greens: 0,
      libs: 0,
      wi: 0,
    },
  })

  const [isOpen, handleOpen] = useState(false);

  return (
    <div className="App">
      <header className="App-header">
        <h2>"Pactómetro" de las Elecciones Estadounidenses 2020</h2>
        <Button
          style={{marginTop: "20px"}}
          icon="info-sign"
          onClick={()=>handleOpen(true)}
          large={true}
          text={"DISCLAIMER"}
          intent={Intent.PRIMARY}
        />
        <div className="general-container">
          <MapContainer states={states} setStates={setStates}/>
          <ResultShower states={states} />
        </div>
        
        <Drawer 
          icon="info-sign"
          isOpen={isOpen}
          onClose={()=>handleOpen(false)}
          size={Drawer.SIZE_SMALL}
          title={"DISCLAIMER"}
          className="bp3-dark disclaimer"
        >
          <div>
            En este simulador los datos son introducidos enteramente por el usuario, para poder ver posibles resultados electorales e imaginar quien puede ser el nuevo presidente electo de los Estados Unidos.
          </div>
          <div>
            El simulador toma los porcentajes de votos introducidos para cada estado y muestra los votos del <a href="https://es.wikipedia.org/wiki/Colegio_Electoral_de_los_Estados_Unidos">colegio electoral</a> proporcionados a ese estado.
          </div>
          <div>
            Este simulador no proporciona una simulación exacta de la atribución de votos electorales de los estados de Maine[ME] y Nebraska[NE], pues estos realizan una distribución casi proporcional de los votos según los distritos electorales de los mismo. Por lo tanto, cualquier simulación, incluso si los datos son completamente correctos, tendrá hasta 5 votos electorales de diferencia con los resultados del colegio reales.
          </div>
        </Drawer>
        <div className="signature">
          Página creada por Mateo García <a href="https://twitter.com/KratsoZerusa"><i className="fab fa-twitter-square" /> @KratsoZerusa</a> <a href="https://www.twitch.tv/kratsozerusa"><i className="fab fa-twitch"/> kratsozerusa</a>
        </div>
      </header>
    </div>
  );
}

export default App;
