import { MultiSlider } from "@blueprintjs/core";
import { DEMOCRAT, GREEN, LIBERTARIAN, NO_CONTEST, REPUBLICAN, WRITE_IN } from "../form/MapContainer";
import { Winner } from "./Winner";

const stateValues = {
	AL: 9,
	AK: 3,
	AZ: 11,
	AR: 6,
	CA: 55,
	CO: 9,
	CT: 7,
	DE: 3,
	DC: 3,
	FL: 29,
	GA: 16,
	HI: 4,
	ID: 4,
	IL: 20,
	IN: 11,
	IA: 6,
	KS: 6,
	KY: 8,
	LA: 8,
	ME: 4,
	MD: 10,
	MA: 11,
	MI: 16,
	MN: 10,
	MS: 6,
	MO: 10,
	MT: 3,
	NE: 5,
	NV: 6,
	NH: 4,
	NJ: 14,
	NM: 5,
	NY: 29,
	NC: 15,
	ND: 3,
	OH: 18,
	OK: 7,
	OR: 7,
	PA: 20,
	RI: 4,
	SC: 9,
	SD: 3,
	TN: 11,
	TX: 38,
	UT: 6,
	VT: 3,
	VA: 13,
	WA: 12,
	WV: 5,
	WI: 10,
	WY: 3,
}


export function ResultsParser(props) {

	const dems = props.dems.map(state => stateValues[state]).reduce((prev, curr) => prev + curr, 0)
	const reps = props.reps.map(state => stateValues[state]).reduce((prev, curr) => prev + curr, 0)
	const greens = props.greens.map(state => stateValues[state]).reduce((prev, curr) => prev + curr, 0)
	const libs = props.libs.map(state => stateValues[state]).reduce((prev, curr) => prev + curr, 0)
	const wi = props.wi.map(state => stateValues[state]).reduce((prev, curr) => prev + curr, 0)

	return (
		<div>
			<MultiSlider 
				disabled
				max={538}
				min={0}
			>
				<MultiSlider.Handle value={dems} trackStyleBefore={{backgroundColor: DEMOCRAT}} />
				<MultiSlider.Handle value={dems + reps} trackStyleBefore={{backgroundColor: REPUBLICAN}} />
				<MultiSlider.Handle value={dems + reps + greens} trackStyleBefore={{backgroundColor: GREEN}} />
				<MultiSlider.Handle value={dems + reps + greens + libs} trackStyleBefore={{backgroundColor: LIBERTARIAN}} />
				<MultiSlider.Handle value={dems + reps + greens + libs + wi} trackStyleBefore={{backgroundColor: WRITE_IN}} trackStyleAfter={{backgroundColor: NO_CONTEST}} />
			</MultiSlider>

			<Winner dems={dems} reps={reps} greens={greens} libs={libs} wi={wi} />
		</div>
	)
}