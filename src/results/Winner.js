import biden from '../map/1200px-Biden_Harris_logo.svg.png'
import trump from '../map/61+U0nOesUL._AC_SL1000_.jpg'
import jorgensen from '../map/Jo_Jorgensen_2020_campaign_logo_2.png'
import walker from '../map/Hawkins_Walker_Logo.png'
import whomegalul from '../map/439px-Question_mark.svg.png'

export function Winner(props) {

	let image;

	const max = Math.max(props.dems, props.reps, props.libs, props.greens, props.wi)

	if (max === props.dems) {
		image = biden
	} else if (max === props.libs) {
		image = jorgensen
	} else if (max === props.greens) {
		image = walker
	} else if (max === props.reps ) {
		image = trump
	} else {
		image = whomegalul
	}

	if(max === 0) {
		image = ""
	}

	return (
		<div>
			<h3>Ganador</h3>
			<img src={image} width="200px" alt=""/>
		</div>
	)
}