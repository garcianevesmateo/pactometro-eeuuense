import { SvgLoader } from 'react-svgmt'
import * as React from 'react'
import { results } from '../map/stringCollege'
import { ResultsParser } from './ResultsParser'

export class ResultShower extends React.Component {

	render() {
		const dems = Object.keys(this.props.states).filter((key) => {
			return Math.max(this.props.states[key].dems, this.props.states[key].reps, this.props.states[key].libs, this.props.states[key].greens, this.props.states[key].wi) === this.props.states[key].dems && this.props.states[key].dems !== 0
		})
		const reps = Object.keys(this.props.states).filter((key) => {
			return Math.max(this.props.states[key].dems, this.props.states[key].reps, this.props.states[key].libs, this.props.states[key].greens, this.props.states[key].wi) === this.props.states[key].reps && this.props.states[key].reps !== 0
		})

		const libs = Object.keys(this.props.states).filter((key) => {
			return Math.max(this.props.states[key].dems, this.props.states[key].reps, this.props.states[key].libs, this.props.states[key].greens, this.props.states[key].wi) === this.props.states[key].libs && this.props.states[key].libs !== 0
		})

		const greens = Object.keys(this.props.states).filter((key) => {
			return Math.max(this.props.states[key].dems, this.props.states[key].reps, this.props.states[key].libs, this.props.states[key].greens, this.props.states[key].wi) === this.props.states[key].greens && this.props.states[key].greens !== 0
		})

		const wi = Object.keys(this.props.states).filter((key) => {
			return Math.max(this.props.states[key].dems, this.props.states[key].reps, this.props.states[key].libs, this.props.states[key].greens, this.props.states[key].wi) === this.props.states[key].wi && this.props.states[key].wi !== 0
		})

		return (
			<div className="results">
				<SvgLoader svgXML={results} />
				<ResultsParser dems={dems} reps={reps} libs={libs} greens={greens} wi={wi} />
			</div>
		)
	}
}